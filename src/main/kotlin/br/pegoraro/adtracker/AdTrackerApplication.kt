package br.pegoraro.adtracker

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class AdTrackerApplication

fun main(args: Array<String>) {
    SpringApplication.run(AdTrackerApplication::class.java, *args)
}