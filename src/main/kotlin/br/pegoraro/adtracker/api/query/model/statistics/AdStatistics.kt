package br.pegoraro.adtracker.api.query.model.statistics

import br.pegoraro.adtracker.api.query.model.QueryResult

data class AdStatistics(val interval : Interval, val stats: Statistics) : QueryResult