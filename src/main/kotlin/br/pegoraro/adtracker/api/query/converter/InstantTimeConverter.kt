package br.pegoraro.adtracker.api.query.converter

import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Service
import java.time.DateTimeException
import java.time.Instant
import java.time.format.DateTimeFormatter

@Service
class InstantTimeConverter : Converter<String, Instant> {


    override fun convert(source: String): Instant? {
        if(source.isNotEmpty()) {
            return Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(source))
        }
        throw DateTimeException("DateTime field must be in ISO-8601 format")
    }


}