package br.pegoraro.adtracker.api.query.model.grouping

import br.pegoraro.adtracker.api.query.model.QueryResult
import br.pegoraro.adtracker.api.query.model.statistics.Interval

data class GroupingStats(val interval: Interval, val data: List<AdGroupedStatistics>) : QueryResult {
}