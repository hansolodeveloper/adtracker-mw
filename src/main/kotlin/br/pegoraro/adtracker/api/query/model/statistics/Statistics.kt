package br.pegoraro.adtracker.api.query.model.statistics

data class Statistics(val deliveries : Long, val clicks: Long, val installs: Long)