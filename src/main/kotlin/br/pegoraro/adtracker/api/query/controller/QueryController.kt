package br.pegoraro.adtracker.api.query.controller

import br.pegoraro.adtracker.api.query.service.QueryService
import br.pegoraro.adtracker.api.query.model.QueryResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.Instant
import java.time.format.DateTimeFormatter

@RestController
@RequestMapping("ads/statistics")
class QueryController(val queryService: QueryService) {

    @GetMapping
    fun adStatistics(@RequestParam start: String, @RequestParam end: String, @RequestParam(required = false, name = "group_by") groupBy: List<Category> ) : QueryResult {
        val startInstant : Instant = Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(start))
        val endInstant : Instant = Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(end))
        if(groupBy.isEmpty()) {
            return queryService.fetchPlainStatistics(startInstant, endInstant)
        }
        return queryService.fetchGroupedStatistics(startInstant,endInstant, groupBy)

    }

}