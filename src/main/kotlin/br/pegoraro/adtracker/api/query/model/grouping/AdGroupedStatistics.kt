package br.pegoraro.adtracker.api.query.model.grouping

import br.pegoraro.adtracker.api.query.model.QueryResult
import br.pegoraro.adtracker.api.query.model.statistics.Statistics

data class AdGroupedStatistics(val fields : RestrictionGroup, val stats: Statistics) : QueryResult {
}