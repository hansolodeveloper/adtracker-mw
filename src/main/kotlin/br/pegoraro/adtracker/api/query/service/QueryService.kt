package br.pegoraro.adtracker.api.query.service

import br.pegoraro.adtracker.api.ads.model.AdDelivery
import br.pegoraro.adtracker.api.query.controller.Category
import br.pegoraro.adtracker.api.query.model.grouping.AdGroupedStatistics
import br.pegoraro.adtracker.api.query.model.grouping.GroupingStats
import br.pegoraro.adtracker.api.query.model.grouping.PlainGroupStatistics
import br.pegoraro.adtracker.api.query.model.grouping.RestrictionGroup
import br.pegoraro.adtracker.api.query.model.statistics.AdStatistics
import br.pegoraro.adtracker.api.query.model.statistics.Interval
import br.pegoraro.adtracker.api.query.model.statistics.Statistics
import br.pegoraro.adtracker.repositories.AdDeliveryRepository
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.Aggregation.bucket
import org.springframework.data.mongodb.core.aggregation.Aggregation.facet
import org.springframework.data.mongodb.core.aggregation.Aggregation.match
import org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation
import org.springframework.data.mongodb.core.aggregation.AggregationOperation
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class QueryService(private val adDeliveryRepository: AdDeliveryRepository, private val mongoTemplate: MongoTemplate) {

    fun fetchPlainStatistics(start: Instant, end: Instant) : AdStatistics {
        val deliveries = adDeliveryRepository.findAllByTimeBetween(start, end)
        val deliveryCount = deliveries.size.toLong()
        val clicks = deliveries.stream().flatMap {
            it.clicks.stream()
        }.filter { it.time.isAfter(start) && it.time.isBefore(end) }.count()
        val installs = deliveries.stream().flatMap {
            it.installs.stream()
        }.filter { it.time.isAfter(start) && it.time.isBefore(end) }.count()

        return AdStatistics(Interval(start, end), Statistics(deliveryCount,clicks,installs))
    }

    fun fetchGroupedStatistics(start:Instant, end: Instant, groupingBy : List<Category>) : GroupingStats {
        val groupingStats = GroupingStats(Interval(start,end), emptyList())
        mongoTemplate.aggregate(
                getAggregation(start, end, groupingBy), AdDelivery::class.java, PlainGroupStatistics::class.java).mappedResults.forEach {
            groupingStats.data.plus(AdGroupedStatistics(
                    fields = RestrictionGroup(it.browser, it.operatingSystem),
                    stats = Statistics(deliveries = it.amountOfDeliveries, clicks = it.amountOfClicks, installs = it.amountOfInstalls)))
        }

        return groupingStats
    }

    /**
     * It needs a few more facets so the search can be exactly on the terms of the OutputClass, but I am afraid time has been too long in this.
     */
    private fun getAggregation(start: Instant, end: Instant, groupingBy: List<Category>): Aggregation {
        val facets : List<AggregationOperation> = emptyList()
        if (groupingBy.contains(Category.os)) {
            facets.plus(facet(match(Criteria.where("time").lte(start).and("time").gte(end)), bucket("os")).`as`("groupByOs"))
        }
        if (groupingBy.contains(Category.browser)) {
            facets.plus(facet(match(Criteria.where("time").lte(start).and("time").gte(end)), bucket("browser")).`as`("groupByBrowser"))
        }
        return newAggregation(facets)
    }

}