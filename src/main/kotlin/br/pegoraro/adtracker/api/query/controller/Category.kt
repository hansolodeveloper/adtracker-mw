package br.pegoraro.adtracker.api.query.controller

enum class Category {
    browser, os
}