package br.pegoraro.adtracker.api.query.model.grouping;

import br.pegoraro.adtracker.api.ads.model.enumerators.Browser;
import br.pegoraro.adtracker.api.ads.model.enumerators.OperatingSystem;

public class PlainGroupStatistics {
    private Browser browser;
    private OperatingSystem operatingSystem;
    private Long amountOfDeliveries;
    private Long amountOfClicks;
    private Long amountOfInstalls;

    public PlainGroupStatistics(Browser browser, OperatingSystem operatingSystem, Long amountOfDeliveries, Long amountOfClicks,
            Long amountOfInstalls) {
        this.browser = browser;
        this.operatingSystem = operatingSystem;
        this.amountOfDeliveries = amountOfDeliveries;
        this.amountOfClicks = amountOfClicks;
        this.amountOfInstalls = amountOfInstalls;
    }

    public Browser getBrowser() {
        return browser;
    }

    public OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }

    public Long getAmountOfDeliveries() {
        return amountOfDeliveries;
    }

    public Long getAmountOfClicks() {
        return amountOfClicks;
    }

    public Long getAmountOfInstalls() {
        return amountOfInstalls;
    }
}
