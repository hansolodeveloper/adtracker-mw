package br.pegoraro.adtracker.api.query.model.grouping

import br.pegoraro.adtracker.api.ads.model.enumerators.Browser
import br.pegoraro.adtracker.api.ads.model.enumerators.OperatingSystem

data class RestrictionGroup(val browser : Browser?, val os: OperatingSystem?)