package br.pegoraro.adtracker.api.query.model.statistics

import java.time.Instant

data class Interval(val start: Instant, val end: Instant)