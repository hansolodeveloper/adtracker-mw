package br.pegoraro.adtracker.api.ads.validators

import br.pegoraro.adtracker.api.ads.model.AdDelivery
import br.pegoraro.adtracker.contracts.Validator
import org.springframework.stereotype.Service
import org.springframework.web.client.RestClientException

@Service
open class SourceWhitelistValidator: Validator<AdDelivery> {

    private val regexValidator = "(\\w+).(?:com|net)"

    override fun validate(value: AdDelivery) {
        if(!value.site.matches(Regex.fromLiteral(regexValidator))) {
            throw RestClientException("Site delivered ad not compatible with whitelist guidelines.")
        }
    }
}