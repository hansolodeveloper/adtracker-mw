package br.pegoraro.adtracker.api.ads.processors

import br.pegoraro.adtracker.api.ads.model.AdInstall
import br.pegoraro.adtracker.contracts.Processor
import br.pegoraro.adtracker.repositories.AdDeliveryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AdInstallProcessor @Autowired constructor(private val adDeliveryRepository: AdDeliveryRepository) : Processor<AdInstall> {
    override fun process(value: AdInstall): AdInstall {
        adDeliveryRepository.findById(value.deliveryId)
                .ifPresent {
                    it.installs.plus(value)
                    adDeliveryRepository.save(it)
                }
        return value
    }
}