package br.pegoraro.adtracker.api.ads.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class AdInstall(val deliveryId: String,
                     @Id val installId: String,
                     val time: Instant)