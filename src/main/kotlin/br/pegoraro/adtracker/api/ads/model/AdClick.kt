package br.pegoraro.adtracker.api.ads.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class AdClick(val deliveryId: String,
                   @Id val clickId: String,
                   val time: Instant)
