package br.pegoraro.adtracker.api.ads.model.enumerators

enum class Browser {
    CHROME, FIREFOX, SAFARI, OPERA, EDGE,
    IE;

}