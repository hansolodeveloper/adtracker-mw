package br.pegoraro.adtracker.api.ads.processors

import br.pegoraro.adtracker.api.ads.model.AdClick
import br.pegoraro.adtracker.contracts.Processor
import br.pegoraro.adtracker.repositories.AdDeliveryRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AdClickProcessor @Autowired constructor(private val adDeliveryRepository: AdDeliveryRepository) : Processor<AdClick> {
    val logger: Logger = LoggerFactory.getLogger(AdClickProcessor::class.java)

    override fun process(value: AdClick): AdClick {
        adDeliveryRepository.findById(value.deliveryId)
                .ifPresent {
                    it.clicks.plus(value)
                    adDeliveryRepository.save(it)
                }

        return value
    }
}