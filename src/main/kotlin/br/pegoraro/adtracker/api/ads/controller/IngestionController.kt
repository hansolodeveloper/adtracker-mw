package br.pegoraro.adtracker.api.ads.controller

import br.pegoraro.adtracker.api.ads.model.AdClick
import br.pegoraro.adtracker.api.ads.model.AdDelivery
import br.pegoraro.adtracker.api.ads.model.AdDeliveryVO
import br.pegoraro.adtracker.api.ads.model.AdInstall
import br.pegoraro.adtracker.api.ads.model.enumerators.Browser
import br.pegoraro.adtracker.api.ads.model.enumerators.OperatingSystem
import br.pegoraro.adtracker.api.ads.processors.AdClickProcessor
import br.pegoraro.adtracker.api.ads.processors.AdDeliveryProcessor
import br.pegoraro.adtracker.api.ads.processors.AdInstallProcessor
import br.pegoraro.adtracker.contracts.Transformer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.util.ClassUtils
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RequestMapping("/ads")
@RestController
class IngestionController
    constructor(private val adDeliveryProcessor: AdDeliveryProcessor, private val adClickProcessor: AdClickProcessor, private val adInstallProcessor: AdInstallProcessor)
    : Transformer<AdDelivery, AdDeliveryVO> {
    val log : Logger = LoggerFactory.getLogger(IngestionController::class.java)

    @PostMapping("/delivery", consumes = ["application/*"])
    fun deliverAd(@RequestBody adEntry: AdDeliveryVO) : Mono<AdDelivery> {
        log.debug("Delivered ad at {}", adEntry)
        return Mono.just(adDeliveryProcessor.process(transform(adEntry)))
    }

    @PostMapping("/click", consumes = ["application/*"])
    fun clicked(@RequestBody adClick : AdClick) : Mono<AdClick> {
        return Mono.just(adClickProcessor.process(adClick))
    }

    @PostMapping("/install", consumes = ["application/*"])
    fun clicked(@RequestBody adInstall : AdInstall) : Mono<AdInstall> {
        return Mono.just(adInstallProcessor.process(adInstall))
    }

    override fun transform(value : AdDeliveryVO) : AdDelivery {
        return AdDelivery(value.advertisementId, value.deliveryId, value.time, Browser.valueOf(value.browser.toUpperCase()),
                OperatingSystem.valueOf(value.os.toUpperCase()), value.site, emptySet(), emptySet())
    }

}