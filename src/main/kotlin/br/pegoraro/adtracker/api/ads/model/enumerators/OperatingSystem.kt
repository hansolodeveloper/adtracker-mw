package br.pegoraro.adtracker.api.ads.model.enumerators

enum class OperatingSystem {
    LINUX, OSX, IOS, ANDROID, WINDOWS, OTHER
}