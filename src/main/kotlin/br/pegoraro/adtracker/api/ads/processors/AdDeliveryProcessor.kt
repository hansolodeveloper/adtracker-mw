package br.pegoraro.adtracker.api.ads.processors

import br.pegoraro.adtracker.api.ads.model.AdDelivery
import br.pegoraro.adtracker.contracts.Processor
import br.pegoraro.adtracker.contracts.Validator
import br.pegoraro.adtracker.repositories.AdDeliveryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AdDeliveryProcessor constructor(private val sourceWhitelistValidator: Validator<AdDelivery> , @Autowired val adDeliveryRepository: AdDeliveryRepository) : Processor<AdDelivery> {

    override fun process(value: AdDelivery): AdDelivery {
        sourceWhitelistValidator.validate(value)
        adDeliveryRepository.save(value)
        return value
    }
}