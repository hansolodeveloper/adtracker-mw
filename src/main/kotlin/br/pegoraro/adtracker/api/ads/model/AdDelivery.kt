package br.pegoraro.adtracker.api.ads.model

import br.pegoraro.adtracker.api.ads.model.enumerators.Browser
import br.pegoraro.adtracker.api.ads.model.enumerators.OperatingSystem
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class AdDelivery(
        val advertisementId: Long,
        @Id val deliveryId: String,
        val time: Instant,
        val browser: Browser,
        val os: OperatingSystem,
        val site: String,
        @DBRef val clicks: Set<AdClick>,
        @DBRef val installs : Set<AdInstall>) {
    override fun toString() : String {
        return "{ advertisementId: $advertisementId, " +
                "deliveryId: $deliveryId," +
                "time: $time," +
                "browser: $browser," +
                "os: $os," +
                "site: $site }"
    }
}

