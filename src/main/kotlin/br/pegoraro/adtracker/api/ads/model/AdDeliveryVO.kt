package br.pegoraro.adtracker.api.ads.model

import org.springframework.data.annotation.Id
import java.time.Instant

data class AdDeliveryVO(
        val advertisementId: Long,
        @Id val deliveryId: String,
        val time: Instant,
        val browser: String,
        val os: String,
        val site: String) {
}