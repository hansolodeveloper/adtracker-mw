package br.pegoraro.adtracker.contracts

interface Processor<T> {
    fun process(value: T) : T
}