package br.pegoraro.adtracker.contracts

interface Validator<T> {
    fun validate(value : T)
}