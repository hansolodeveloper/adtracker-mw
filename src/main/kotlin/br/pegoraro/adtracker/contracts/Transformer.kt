package br.pegoraro.adtracker.contracts

interface Transformer<E, V> {
    fun transform(value: V) : E
}