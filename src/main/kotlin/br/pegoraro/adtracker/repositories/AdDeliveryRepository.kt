package br.pegoraro.adtracker.repositories

import br.pegoraro.adtracker.api.ads.model.AdDelivery
import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface AdDeliveryRepository : CrudRepository<AdDelivery, String>{
    fun findAllByTimeBetween(start : Instant, end: Instant) : List<AdDelivery>
}
