package br.pegoraro.adtracker.api.ads

import br.pegoraro.adtracker.api.ads.controller.IngestionController
import br.pegoraro.adtracker.api.ads.model.AdClick
import br.pegoraro.adtracker.api.ads.model.AdDelivery
import br.pegoraro.adtracker.api.ads.model.AdDeliveryVO
import br.pegoraro.adtracker.api.ads.model.AdInstall
import br.pegoraro.adtracker.api.ads.model.enumerators.Browser
import br.pegoraro.adtracker.api.ads.model.enumerators.OperatingSystem
import br.pegoraro.adtracker.api.ads.processors.AdClickProcessor
import br.pegoraro.adtracker.api.ads.processors.AdDeliveryProcessor
import br.pegoraro.adtracker.api.ads.processors.AdInstallProcessor
import br.pegoraro.adtracker.api.ads.validators.SourceWhitelistValidator
import br.pegoraro.adtracker.repositories.AdDeliveryRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.client.RestClientException
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.stream.Stream

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class AdIngestionIntegratedSpecification extends Specification {

    @Shared
    AdClickProcessor adClickProcessor

    @Shared
    AdInstallProcessor adInstallProcessor

    @Shared
    AdDeliveryProcessor adDeliveryProcessor

    @Shared
    AdDeliveryRepository adDeliveryRepository

    @Shared
    IngestionController ingestionController

    @Shared
    MockMvc mockMvc

    @Shared
    ObjectMapper objectMapper

    private SourceWhitelistValidator sourceWhitelistValidator

    void setup() {
        sourceWhitelistValidator = Mock(SourceWhitelistValidator)
        adDeliveryRepository = Mock(AdDeliveryRepository)
        def streamOfValidators = Stream.of(sourceWhitelistValidator)
        adClickProcessor = new AdClickProcessor(adDeliveryRepository)
        adInstallProcessor = new AdInstallProcessor(adDeliveryRepository)
        adDeliveryProcessor = new AdDeliveryProcessor(streamOfValidators, adDeliveryRepository)
        ingestionController = new IngestionController(adDeliveryProcessor, adClickProcessor, adInstallProcessor)
        objectMapper = Jackson2ObjectMapperBuilder.json().build()
        mockMvc = MockMvcBuilders.standaloneSetup(ingestionController)
                .build()
    }

    @Ignore("TODO fix the media type error")
    def "POST /ads/delivery - Return 200 Scenario"() {
        given: "a AdEntry"
        def adEntry = new AdDelivery(4483L, UUID.nameUUIDFromBytes("acme-delivered" as byte[]).toString(),
                // with provided date pattern, only added `:` in the timezone, because it was failing
                Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse("2018-01-07T18:32:23.602300+00:00")),
                Browser.FIREFOX, OperatingSystem.ANDROID, "www.acme.com", [] as Set<AdClick>, [] as Set<AdInstall>)
        when: "the ingestionController receives an post with correct ad delivery payload"
        def adDelivery = new AdDeliveryVO(4483L, UUID.nameUUIDFromBytes("acme-delivered" as byte[]).toString(),
                // with provided date pattern, only added `:` in the timezone, because it was failing
                Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse("2018-01-07T18:32:23.602300+00:00")),
                "FIREFOX", "ANDROID", "www.acme.com")
        def jsonDelivery = objectMapper.writeValueAsString(adDelivery)
        def response = mockMvc.perform(post("/ads/delivery")
                .accept("application/*")
                .content(jsonDelivery))
        then: "the delivered ad should be persisted in the Repository with all validations must be called and be successful"
        1 * sourceWhitelistValidator.validate(_ as AdDelivery)
        1 * adDeliveryRepository.findById(_ as String) >> Optional.of(adEntry)
        1 * adDeliveryRepository.save(adEntry)
        response.andExpect(status().isOk())
        response.andExpect(content().json(jsonDelivery))

    }
    @Ignore("TODO fix the media type error")
    def "POST /ads/delivery - Return 500 Scenario"() {
        given: "a AdEntry"
        when: "the ingestionController receives an post with correct ad delivery payload"
        def adDelivery = new AdDeliveryVO(4483L, UUID.nameUUIDFromBytes("acme-delivered" as byte[]).toString(),
                // with provided date pattern, only added `:` in the timezone, because it was failing
                Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse("2018-01-07T18:32:23.602300+00:00")),
                "Chrome", "Linux", "www.acme.com")
        def response = mockMvc.perform(post("/ads/delivery")
                .accept("application/*")
                .content(objectMapper.writeValueAsString(adDelivery)))
        then: "the delivered ad should be persisted in the Repository with all validations must be called and be successful"
        1 * sourceWhitelistValidator.validate(_) >> { throw new RestClientException("Some error occurred") }
        response.andExpect(status().isInternalServerError())
        response.andExpect(content().string("Some error occurred"))
    }

    @Ignore("TODO fix the media type error")
    def "POST /ads/install - Return 200 Scenario"() {
        given: "a AdEntry"
        def adEntry = new AdDelivery(4483L, UUID.nameUUIDFromBytes("acme-delivered" as byte[]).toString(),
                // with provided date pattern, only added `:` in the timezone, because it was failing
                Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse("2018-01-07T18:32:23.602300+00:00")),
                Browser.FIREFOX, OperatingSystem.ANDROID, "www.acme.com", [] as Set<AdClick>, [] as Set<AdInstall>)
        when: "the ingestionController receives an post with correct ad delivery payload"
        def adInstall = new AdInstall(UUID.randomUUID().toString(), UUID.randomUUID().toString(), Instant.now())
        def json = objectMapper.writeValueAsString(adInstall)
        def response = mockMvc.perform(post("/ads/install")
                .accept("application/*")
                .content(json))
        then: "the installed ad should be persisted in the Repository with all validations must be called and be successful"
        1 * adDeliveryRepository.findOne() >> Optional.of(adEntry)
        1 * adDeliveryRepository.save(adEntry)
        response.andExpect(status().isOk())
        response.andExpect(content().json(json))

    }

    @Ignore("TODO fix the media type error")
    def "POST /ads/install - Return 404 Scenario"() {
        when: "the ingestionController receives an post with correct ad delivery payload"
        def adInstall = new AdInstall(UUID.randomUUID().toString(), UUID.randomUUID().toString(), Instant.now())
        def response = mockMvc.perform(post("/ads/install")
                .accept("application/*")
                .content(objectMapper.writeValueAsString(adInstall)))
        then: "the installed ad should be persisted in the Repository with all validations must be called and be successful"
        1 * adDeliveryRepository.findOne() >> Optional.empty()
        response.andExpect(status().isNotFound())
    }

    @Ignore("TODO fix the media type error")
    def "POST /ads/install - Return 500 Scenario"() {
        when: "the ingestionController receives an post with correct ad delivery payload"
        def adInstall = new AdInstall(UUID.randomUUID().toString(), UUID.randomUUID().toString(), Instant.now())
        def response = mockMvc.perform(post("/ads/install")
                .accept("application/*")
                .content(objectMapper.writeValueAsString(adInstall)))
        then: "the installed ad should be persisted in the Repository with all validations must be called and be successful"
        1 * adDeliveryRepository.findById(_ as String) >> { throw new RestClientException("Some error occurred")}
        response.andExpect(status().isInternalServerError())
        response.andExpect(content().string("Some error occurred"))
    }

    @Ignore("TODO fix the media type error")
    def "POST /ads/click - Return 200 Scenario"() {
        given: "a AdEntry"
        def adEntry = new AdDelivery(4483L, UUID.nameUUIDFromBytes("acme-delivered" as byte[]).toString(),
                // with provided date pattern, only added `:` in the timezone, because it was failing
                Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse("2018-01-07T18:32:23.602300+00:00")),
                Browser.FIREFOX, OperatingSystem.ANDROID, "www.acme.com", [] as Set<AdClick>, [] as Set<AdInstall>)
        when: "the ingestionController receives an post with correct ad delivery payload"
        def adClick = new AdClick(UUID.randomUUID().toString(), UUID.randomUUID().toString(), Instant.now())
        def json = objectMapper.writeValueAsString(adClick)
        def response = mockMvc.perform(post("/ads/click")
                .accept("application/*")
                .content(json))
        then: "the clicked ad should be persisted in the Repository with all validations must be called and be successful"
        1 * adDeliveryRepository.findById(_ as String) >> Optional.of(adEntry)
        1 * adDeliveryRepository.save(adEntry)
        response.andExpect(status().isOk())
        response.andExpect(content().json(json))

    }

    @Ignore("TODO fix the media type error")
    def "POST /ads/click - Return 404 Scenario"() {
        when: "the ingestionController receives an post with correct ad delivery payload"
        def adClick = new AdClick(UUID.randomUUID().toString(), UUID.randomUUID().toString(), Instant.now())
        def response = mockMvc.perform(post("/ads/click")
                .accept("application/*")
                .content(objectMapper.writeValueAsString(adClick)))
        then: "the clicked ad should be persisted in the Repository with all validations must be called and be successful"
        1 * adDeliveryRepository.findById(_ as String)>> Optional.empty()
        response.andExpect(status().isNotFound())
    }

    @Ignore("TODO fix the media type error")
    def "POST /ads/click - Return 500 Scenario"() {
        when: "the ingestionController receives an post with correct ad delivery payload"
        def adClick = new AdClick(UUID.randomUUID().toString(), UUID.randomUUID().toString(), Instant.now())
        def response = mockMvc.perform(post("/ads/click")
                .accept("application/*")
                .content(objectMapper.writeValueAsString(adClick)))
        then: "the clicked ad should be persisted in the Repository with all validations must be called and be successful"
        response.andExpect(status().isInternalServerError())
        response.andExpect(content().string("Some error occurred"))
    }
}
