package br.pegoraro.adtracker.api.query.service

import br.pegoraro.adtracker.api.ads.model.AdClick
import br.pegoraro.adtracker.api.ads.model.AdDelivery
import br.pegoraro.adtracker.api.ads.model.AdInstall
import br.pegoraro.adtracker.api.ads.model.enumerators.Browser
import br.pegoraro.adtracker.api.ads.model.enumerators.OperatingSystem
import br.pegoraro.adtracker.api.query.controller.Category
import br.pegoraro.adtracker.api.query.controller.QueryController
import br.pegoraro.adtracker.api.query.model.grouping.GroupingStats
import br.pegoraro.adtracker.api.query.model.statistics.AdStatistics
import br.pegoraro.adtracker.repositories.AdDeliveryRepository
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

import java.time.Instant
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

class QueryServiceSpecification extends Specification {

    @Shared
    QueryController queryController

    @Shared
    QueryService queryService

    @Shared
    AdDeliveryRepository adDeliveryRepository

    @Shared
    MongoTemplate mongoTemplate

    void setup() {
        mongoTemplate = Mock(MongoTemplate)
        adDeliveryRepository = Mock(AdDeliveryRepository)
        queryService = new QueryService(adDeliveryRepository,mongoTemplate)
        queryController = new QueryController(queryService)
    }

    def "FetchPlainStatistics"() {
        given: "query parameters"
        def startInstant = OffsetDateTime.now().minusSeconds(3600)
        def start = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(startInstant)
        def endInstant = OffsetDateTime.now()
        def end = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(endInstant)
        and: "the expected data"
        def firstDelivery = UUID.randomUUID().toString()
        def secondDelivery = UUID.randomUUID().toString()
        def deliveries = [
                new AdDelivery(123L, firstDelivery,Instant.now().minusSeconds(3595), Browser.CHROME, OperatingSystem.ANDROID, "acme.com",
                        [ new AdClick(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(3592)),
                          new AdClick(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(3800))] as Set,
                        [ new AdInstall(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(3591)),
                          new AdInstall(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(3590)),
                          new AdInstall(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(3755))] as Set)
                ,
                new AdDelivery(123L, secondDelivery,Instant.now().minusSeconds(151), Browser.CHROME, OperatingSystem.ANDROID, "acme.com",
                        [ new AdClick(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(192)),
                          new AdClick(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(192))] as Set,
                        [ new AdInstall(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(191)),
                          new AdInstall(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(190)),
                          new AdInstall(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(185))] as Set)
        ]
        when: "Query containing only time interval"
        AdStatistics response = queryController.adStatistics(start, end, []) as AdStatistics
        then: "The query should contain both deliveries and its respective clicks "
        1 * adDeliveryRepository.findAllByTimeBetween(startInstant.toInstant(), endInstant.toInstant()) >> deliveries
        response.with {
            stats.clicks == 3
            stats.deliveries == 2
            stats.installs == 5
        }
    }

    @Ignore("Some error in Spock is not recognizing the response property")
    def "FetchGroupedStatistics"() {
        given: "query parameters"
        def start = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(OffsetDateTime.now().minusSeconds(3600))
        def end = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(OffsetDateTime.now())
        and: "the expected data"
        def firstDelivery = UUID.randomUUID().toString()
        def secondDelivery = UUID.randomUUID().toString()
        def deliveries = [
                new AdDelivery(123L, firstDelivery,Instant.now().minusSeconds(3595), Browser.FIREFOX, OperatingSystem.ANDROID, "acme.com",
                        [ new AdClick(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(3592)),
                          new AdClick(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(1200))] as Set,
                        [ new AdInstall(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(911)),
                          new AdInstall(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(3590)),
                          new AdInstall(firstDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(581))] as Set)
                ,
                new AdDelivery(123L, secondDelivery,Instant.now().minusSeconds(151), Browser.SAFARI, OperatingSystem.IOS, "acme.com",
                        [ new AdClick(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(88)),
                          new AdClick(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(154)),
                          new AdClick(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(444))] as Set,
                        [ new AdInstall(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(191)),
                          new AdInstall(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(190)),
                          new AdInstall(secondDelivery, UUID.randomUUID().toString(), Instant.now().minusSeconds(1))] as Set)
        ]
        when: "Query containing time interval and browser category"
        GroupingStats response = queryController.adStatistics(start, end, [Category.browser]) as GroupingStats
        then: "The query should contain both deliveries and its respective clicks "
        1 * mongoTemplate.aggregate(_ as Aggregation, _ as Class, _ as Class) >> deliveries
        and: "The response should contain both deliveries and its respective clicks "
        response.with {
            data.with {
                1 * fields.browser == Browser.FIREFOX
                1 * fields.browser == Browser.SAFARI
                1 * stats.with {
                    1 * it.deliveries == 1
                    1 * clicks == 2
                    1 * installs == 3
                }
                1 * stats.with {
                    1 * it.deliveries == 1
                    1 * clicks == 3
                    1 * installs == 3
                }
            }
        }
    }
}
